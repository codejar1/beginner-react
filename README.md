# Gyakorlas

## install

`yarn install`

## start

`yarn start`

## Allapotok

 - Kezdo allapotot a kovetkezo parancsal ered el: `git checkout alap`
 - Componensek elkeszitese: `git checkout components`
 - State es props gyakorlas: `git checkout state_props`
 - Esemeny kezelese: `git checkout events`
 - state beallitasa: `git checkout setState`
 - Ha async state-et kezelnenk fetch vagy timer miatt akkor pelda: `git checkout asyncState`
 - Visszavaltas a vegso, kesz allapotra: `git checkout master`
 - React router pelda `git checkout router`
