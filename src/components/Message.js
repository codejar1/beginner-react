import React, { Component } from 'react';
import avatar from '../imgs/avatar-svgrepo-com.svg'


export default class Message extends Component {

    constructor(props) {
        super(props);

        this.handlePlus = this.handlePlus.bind(this);
        this.handleMinus = this.handleMinus.bind(this);
    }

    handlePlus() {
        console.log('plus');

        this.props.onPlusMinusChange(this.props.id, 'plus');
    }

    handleMinus() {
        console.log('minus')
        this.props.onPlusMinusChange(this.props.id, 'minus');
    }

    render() {
        return (
            <div className="message">
                <div className="person">
                    <img src={avatar} alt='avatar' /> <h5>{this.props.name} <span> {this.props.date || 'no-date'} </span></h5>
                </div>
                <div className="message-body">
                    {this.props.content}
                </div>
                <div className="message-controllers">
                    <span onClick={this.handlePlus}>+ {this.props.plus}</span>
                    <span onClick={this.handleMinus}>- {this.props.minus}</span>
                </div>
            </div>
        );
    }
}