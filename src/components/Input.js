import React, {Component} from 'react';

export default class Input extends Component {

    handleEnter(event) {
        if (event.key === 'Enter') {
            this.props.onEnter(event.target.value);
            event.target.value = '';
        }
    }

    render() {
        return (
            <input type="text" placeholder="Enter some note..." onKeyUp={this.handleEnter.bind(this)} />
        )
    }
}