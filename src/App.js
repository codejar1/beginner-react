import React, { Component } from 'react';
import './App.css';
import {useState} from 'react';
import Input from './components/Input';
import Message from './components/Message';
import Profile from './components/Profile';

function App() {

  const [messages, setMessages] = useState([
    {
      name: 'Someone 1',
      content: 'Hi',
      plus: 0,
      minus: 0,
      date: '2030. 01. 01',
      id: 1
    },
    {
      name: 'Someone 2',
      content: 'Foo bar',
      plus: 0,
      minus: 0,
      date: '2030. 01. 01',
      id: 2
    },
    {
      name: 'Someone 3',
      content: 'Baz',
      plus: 0,
      minus: 0,
      date: '2030. 01. 01',
      id: 3 
    }
  ]);

  function handleInput(value) {
    setMessages([
      {
      name: 'Me',
      date: '2030. 01. 02',
      plus: 0,
      minus: 0,
      content: value,
      id: messages.length + 1
    },
    ...messages,
    ])
  }

  function handlePlusMinusChange(id, plusOrMinus) {
    const message = messages.find(message => message.id === id);
    if (message) {
      message[plusOrMinus]++;
    }
    
    messages.sort( (a,b ) => a.minus - b.minus);
    setMessages([...messages]);
  }

  return (
    <div className="container">
      <div className="messages-container">
        <h1> MyNotes </h1>
        <Input onEnter={handleInput} />
        
        <div className="messages">
          
          {
            !messages.length && <h4>There is no message</h4>
          }

          { messages.map( ( message ) => <Message key={message.id} {...message} onPlusMinusChange={handlePlusMinusChange} /> ) }
        
        </div>
      </div>

      <div className="profile">
        <Profile />
      </div>
    </div>
  );
}

export default App;
